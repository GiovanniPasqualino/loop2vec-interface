import pickle
from code2vec import Code2Vec
import sys
import json
import torch
import random

MAX_LEN = 200
EMBEDDING_DIM = 32
DROPOUT = 0.25
BATCH_SIZE = 1

VOCABS_FOLDER = 'vocabs'


def load_vocabs(path):
    word_vocab = open(path + '/word.pickle', 'rb')
    path_vocab = open(path + '/path.pickle', 'rb')
    return pickle.load(word_vocab), pickle.load(path_vocab)

def pad_contexts(contexts):
    out = []
    for ctx in contexts:
        new_contexts = None
        if len(ctx) < MAX_LEN:
            padded_contexts = ctx
            for _ in range(MAX_LEN-len(ctx)):
                padded_contexts.append(['<pad>', '<pad>', '<pad>'])
            new_contexts = padded_contexts
        else:
            new_contexts = ctx
        out.append(new_contexts)
    return out

def parse_json(json_str):
    json_object = json.loads(json_str)
    res = []
    ctxs = [[c['left'], c['path'], c['right']] for c in json_object['serializers']]

    if len(ctxs) > MAX_LEN:
        ctxs = random.sample(ctxs, MAX_LEN)
    
    res.append(ctxs)
    return pad_contexts(res)

def chunks(seq, size):
    return [seq[i:(i+size)] for i in (range(0, len(seq), size) if len(seq) >= size else range(1))]


word2idx, path2idx = load_vocabs(VOCABS_FOLDER)


def iterate_batch(data, labels):
    for batch in chunks(list(zip(data, labels)), BATCH_SIZE):
        
        tensor_cl = torch.zeros(BATCH_SIZE)
        tensor_l = torch.zeros(BATCH_SIZE, MAX_LEN).long()
        tensor_p = torch.zeros(BATCH_SIZE, MAX_LEN).long()
        tensor_r = torch.zeros(BATCH_SIZE, MAX_LEN).long()

        for i, record in enumerate(batch):
            contexts, label = record
            left_nodes, paths, right_nodes = zip(*contexts)

            left_idxs = [word2idx.get(l, word2idx['<unk>']) for l in left_nodes]
            path_idxs = [path2idx.get(p, path2idx['<unk>']) for p in paths]
            right_idxs = [word2idx.get(r, word2idx['<unk>']) for r in right_nodes]
            
            tensor_cl[i] = label
            tensor_l[i, :] = torch.Tensor(left_idxs).long()
            tensor_p[i, :] = torch.Tensor(path_idxs).long()
            tensor_r[i, :] = torch.Tensor(right_idxs).long()
            
        yield tensor_l, tensor_p, tensor_r


def load_dataset(filename):
    labels = []
    records = []
    with open(filename, 'r') as json_file:
        json_object = json.load(json_file)
        
        for obj in json_object:
            label = int(obj['isParallel'])
            contexts = [[c['left'], c['path'], c['right']] for c in obj['serializers']]
            labels.append(label)
            records.append(contexts)
        return records, labels

content = sys.stdin.readline()
try:
    data = parse_json(content)
except Exception as e:
    print("exception ", content)
    sys.exit(1)

model = Code2Vec(len(word2idx), len(path2idx), EMBEDDING_DIM, DROPOUT)
model.load_state_dict(torch.load('model.pth'))
model.eval()

for tensor_l, tensor_p, tensor_r in iterate_batch(data, [0]):
    prediction = model(tensor_l, tensor_p, tensor_r)
    print('Predicted probability: ', float(prediction[0][0]))