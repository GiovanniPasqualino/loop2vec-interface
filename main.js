const express = require("express")
const app = express()
const path = require('path');
const { spawn } = require('child_process')
const bodyParser = require('body-parser')



const PYTHON = 'python3'
const EXTRACTOR_LOC = 'bin/extractor.jar'
const EVAL_SCRIPT = 'scripts/eval.py'

app.use(express.static(path.join(__dirname, 'Loop2Vec')))

app.get("/", function(req, res) {
    res.sendFile(path.join(__dirname + '/Loop2Vec/index.html'))
});
app.use(
    bodyParser.urlencoded({
      extended: true
    })
  )
  
app.use(bodyParser.json())

app.post("/evaluate", function(req, res, next){
    const message = req.body.input

    const extractor = spawn('java', ['-jar', EXTRACTOR_LOC, '-s'])
    
    extractor.stdin.write(message)

    extractor.stdout.on('data', (data) => {
        try {
            if(data == '\n') return;

            let predictor = spawn(PYTHON, [EVAL_SCRIPT])
            
            
            predictor.stdin.write(data.toString() + '\n')
    
            predictor.stdout.on('data', (data) => {
                console.log('data received from predictor: ' + data)
                res.send(data.toString())
                return
            })
            
            predictor.on('error', function(err) {
                console.log("error: " + err.toString())
                return
            })
            
            predictor.on('exit', (code) => {
                console.log('Predictor exited with exit code ' + code)
            })

            //predict the output
            //display result
        } catch(err) {
            console.log(err)
            // res.send('Syntax error')
            return
        }
        

    })

   /* extractor.on('error', (error) => {
        console.log("Extractor error: " + error.toString())
        return
    })

    extractor.on('exit', (exitStatus) => {
        console.log('extractor exit: ' + exitStatus)
        
        if(exitStatus != 0) {
            res.send('Syntax error on input.')
            return
        }
    })*/
 
})

const port = 5000;

app.listen(port, function() {
    console.log("http://localhost:5000/")
});