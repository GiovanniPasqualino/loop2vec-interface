FROM timbru31/java-node
RUN apt-get update && apt install -y python3-pip && pip3 install torch
WORKDIR /app
COPY . /app
EXPOSE 5000
CMD ["node", "main.js"]