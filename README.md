# Loop2Vec Interface

Loop2Vec Interface è un'interfaccia web che ha lo scopo di assistere l'utente nella ricerca di opportunità di parallelismo. 
In particolare, essa permette di inserire un generico ciclo for in input e di valutare la probabilità che esso sia parallelizzabile. 

# Requisiti
  - Java >= 8
  - Node >= 10
  - Python3 + Pytorch v1.1.0
 
 # Esecuzione
 
 Per avviare l'applicazione basterà eseguire il comando:
 
 ```sh
$ node main.js
```

Il servizio risponderà alla porta 5000.